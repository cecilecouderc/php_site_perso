<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portfolio Cécile</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <header>
        <div class='topHeader'>
            <!-- logo -->
            <a href="home?page=home">
                <img src="/img/cc.png" id="logo" alt="logo" /></a>
                <!-- title site -->
            <h1>Portfolio</h1>
        </div>
        <?php
        include __DIR__ . '/../parts/menu.php'; ?>

    </header>