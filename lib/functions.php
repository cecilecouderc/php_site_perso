<?php
/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent()
{
	if (!isset($_GET['page'])) {
		include __DIR__ . '/../pages/home.php';
	} else {
		$page = $_GET['page'];
		
		switch ($page) {
			case 'home':
				include __DIR__ . '/../pages/home.php';
				break;
			case 'bio':
				include __DIR__ . '/../pages/bio.php';
				break;
			case 'contact':
				include __DIR__ . '/../pages/contact.php';
				break;	
		}
	}
	
}

function getPart($name)
{
	include __DIR__ . '/../parts/' . $name . '.php';
}

function getUserData()
{
	$userData = file_get_contents(__DIR__ . '/../data/user.json');
	$userData = json_decode($userData);

	foreach ($userData as $key => $value) {
		if (is_array($value)) {
			foreach ($value as $value2) {
				echo "<p><br> Year : " . $value2->year . "</p>";
				echo "<p> Company : " . $value2->company . "</p>";
			}
		} else {
			echo "<p>". $value . "<br/></p>";
		}
	}
}

function showLastMessage() {
	$messageSend = file_get_contents(__DIR__ . '/../data/last_message.json');
	$messageSend = json_decode($messageSend);

	foreach ($messageSend as $key => $value) {
		echo "<p>" . $key . " : " . "$value" . "</p>";
	}
}