<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Thanks</title>
</head>

<body>
    <h2>Hello <?php echo htmlspecialchars($_POST['nom']); ?>, thanks for your message!</h2>

    <?php
    $messageSend = array(
        'nom' => $_POST['nom'], 
        'email' => $_POST['email'], 
        'message' => $_POST['message']);
    $file = __DIR__ . "/../data/last_message.json";
    $handle = fopen($file, "w+");
    fwrite($handle, json_encode($messageSend));
    fclose($handle);
    ?>

    <p><a href="home?page=home">Back Home</a><br><br>
       <a href="/admin.php">(Admin : read the last message)</a></p>
</body>

</html>